package edu.khtn.codefinder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CodeDatabase extends SQLiteOpenHelper {
    public static final String TBL_HOME_NAME = "tableHome";
    public static final String TBL_SUB_NAME = "tableLevel";
    public static final String COL_PRI_ID = "priID";
    public static final String COL_NAME = "name";
    public static final String COL_PARENT_ID = "parentID";
    public static final String COL_CHILD_ID = "childID";
    public static final String ARG_PRI_ID = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final String ARG_PARENT_ID = "INTEGER NOT NULL";
    public static final String ARG_CHILD_ID = "INTEGER NOT NULL";
    public static final String ARG_TEXT_NOT = "TEXT NOT NULL";
    public static final int PRI_ID_INDEX = 0;
    public static final int NAME_INDEX = 1;
    public static final int PARENT_ID_INDEX = 2;
    public static final int CHILD_ID_INDEX = 3;
    public static final String DATABASE_NAME = "CodeDatabase.sqlite";
    public static int DATABASE_VERSION = 1;
    public static String RELATIONSHIP = "";
    public static SQLiteDatabase readDB, writeDB;

    public CodeDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        readDB = getReadableDatabase();
        writeDB = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTable(db, TBL_HOME_NAME);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            case 1:
            case 2:
            case 3:
            case 4:
                break;
        }
    }

    public void createTable(SQLiteDatabase db, String tableName) {
        String[] columns = new String[]
                {COL_PRI_ID, COL_NAME, COL_PARENT_ID, COL_CHILD_ID};
        String[] args = new String[]
                {ARG_PRI_ID, ARG_TEXT_NOT, ARG_PARENT_ID + RELATIONSHIP, ARG_CHILD_ID};
        String sql = "CREATE TABLE [" + tableName + "] (";
        for (int i = 0; i < columns.length; i++) {
            sql += columns[i] + " " + args[i];
            if (i < columns.length - 1) {
                sql += ",";
            }
        }
        sql += ")";
        db.execSQL(sql);
    }

    public boolean createTableIfNotExist(String tableName, String relationship) {
        try {
            RELATIONSHIP = relationship;
            createTable(writeDB, tableName);
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    public void insertRecord(String tableName, String name, int parentID, int childID) {
        ContentValues values = new ContentValues();
        values.put(COL_NAME, name);
        values.put(COL_PARENT_ID, parentID);
        values.put(COL_CHILD_ID, childID);
        writeDB.insert(tableName, null, values);
    }

    public void updateRecord(String tableName, String byValue,
                             String whereColumn, String whereValue) {
        ContentValues values = new ContentValues();
        values.put(whereColumn, byValue);
        writeDB.update(tableName, values, whereColumn + " = ?", new String[]{whereValue});
    }

    public void deleteRecord(String tableName, String whereColumn, String whereValue) {
        writeDB.delete(tableName, whereColumn + " = ?", new String[]{whereValue});
    }

    public Cursor selectRecord(String tableName, String whereColumn, String whereValue) {
        String sql = "select * from " + tableName + " where " + whereColumn
                + " = '" + whereValue + "'";
        return writeDB.rawQuery(sql, null);
    }

    public Cursor selectTable(String tableName) {
        String sql = "select * from " + tableName;
        return writeDB.rawQuery(sql, null);
    }

    public Cursor selectTable(SQLiteDatabase db, String tableName) {
        String sql = "select * from " + tableName;
        return db.rawQuery(sql, null);
    }

    public void addColumn(String tableName, String column) {
        String sql = "ALTER TABLE " + tableName + " ADD COLUMN " + column;
        writeDB.execSQL(sql);
    }

    public void dropColumn(String tableName, String column) {
        String sql = "ALTER TABLE " + tableName + " DROP COLUMN " + column;
        writeDB.execSQL(sql);
    }

    public String renameColumn(String tableName, String oldColumn, String newColumn) {
        return "ALTER TABLE " + tableName + " RENAME COLUMN " + oldColumn + " TO " + newColumn;
    }

    public String alterColumn(String tableName, String column, String arg) {
        return "ALTER TABLE " + tableName + " ALTER COLUMN " + column + arg;
    }
}
