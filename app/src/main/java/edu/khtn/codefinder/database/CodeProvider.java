package edu.khtn.codefinder.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

import java.util.List;

public class CodeProvider extends ContentProvider {
    private static final String authority = "edu.khtn.codefinder.provider";
    CodeDatabase database = null;
    SQLiteDatabase dbWrite = null;
    SQLiteDatabase dbRead = null;

    private static final int KEY_HOME_SELECT_TABLE = 1;
    private static final int KEY_HOME_SELECT_PRIID = 2;
    private static final int KEY_SUB_SELECT_TABLE = 11;
    private static final int KEY_SUB_SELECT_PRIID = 12;
    private static final UriMatcher sUriMatcher;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(authority,
                CodeDatabase.TBL_HOME_NAME, KEY_HOME_SELECT_TABLE);
        sUriMatcher.addURI(authority,
                CodeDatabase.TBL_HOME_NAME + "/#", KEY_HOME_SELECT_PRIID);
        sUriMatcher.addURI(authority,
                CodeDatabase.TBL_SUB_NAME, KEY_SUB_SELECT_TABLE);
        sUriMatcher.addURI(authority,
                CodeDatabase.TBL_SUB_NAME + "/#", KEY_SUB_SELECT_PRIID);
    }

    @Override
    public boolean onCreate() {
        database = new CodeDatabase(getContext());
        if (database != null) {
            dbWrite = database.getWritableDatabase();
            dbRead = database.getReadableDatabase();
            return true;
        }
        return false;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        String typeOne = "vnd.android.cursor.item/vnd." + authority + ".";
        String typeMany = "vnd.android.cursor.dir/vnd." + authority + ".";
        String lastSegment = uri.getLastPathSegment();
        int keyValue = sUriMatcher.match(uri);
        if (keyValue == KEY_HOME_SELECT_TABLE || keyValue == KEY_SUB_SELECT_TABLE) {
            return typeMany + lastSegment;
        } else {
            return typeOne + lastSegment;
        }
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        String table = uri.getPathSegments().get(0);
        int keyValue = sUriMatcher.match(uri);
        if (keyValue == KEY_HOME_SELECT_TABLE || keyValue == KEY_SUB_SELECT_TABLE) {
            return dbRead.rawQuery("SELECT * from " + table, null);
        } else {
            System.out.println(table);
            return dbRead.rawQuery("SELECT * from " + table +
                    " where " + selection + " = ?", selectionArgs);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String lastSegment = uri.getLastPathSegment();
        int keyValue = sUriMatcher.match(uri);
        if (keyValue == KEY_HOME_SELECT_TABLE || keyValue == KEY_SUB_SELECT_TABLE) {
            long id = dbWrite.insert(lastSegment, null, values);
            if (id != -1)
                return uri;
            return null;
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String table = uri.getPathSegments().get(0);
        int keyValue = sUriMatcher.match(uri);
        if (keyValue == KEY_HOME_SELECT_TABLE || keyValue == KEY_SUB_SELECT_TABLE) {
            return dbWrite.delete(table, null, null);
        } else {
            return dbWrite.delete(table, selection + " = ?", selectionArgs);
        }
    }

    @Override
    public int update(Uri uri, ContentValues values,
                      String selection, String[] selectionArgs) {
        List segList = uri.getPathSegments();
        String table = (String) segList.get(segList.size() - 2);
        String lastSeg = uri.getLastPathSegment();
        int keyValue = sUriMatcher.match(uri);
        if (keyValue == KEY_HOME_SELECT_TABLE || keyValue == KEY_SUB_SELECT_TABLE) {
            return dbWrite.update(lastSeg, values, database.COL_PRI_ID, null);
        } else {
            return dbWrite.update(table, values,
                    database.COL_PRI_ID + " = ?", new String[]{lastSeg});
        }
    }
}
