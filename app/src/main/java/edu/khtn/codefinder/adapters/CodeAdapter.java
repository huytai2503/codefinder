package edu.khtn.codefinder.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.khtn.codefinder.R;

public class CodeAdapter extends BaseAdapter{
    List list = new ArrayList();
    Context context;

    public CodeAdapter() {
        super();
    }

    public CodeAdapter(Context context) {
        this.context = context;
    }

    public void setList(List list) {
        this.list = list;
    }

    public List getList() {
        return list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.model_text_list, null, false);
        TextView text = (TextView) view.findViewById(R.id.modelText);
        text.setText(getItem(position).toString());
        return view;
    }
}
