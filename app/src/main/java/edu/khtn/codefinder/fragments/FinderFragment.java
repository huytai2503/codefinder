package edu.khtn.codefinder.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.khtn.codefinder.R;
import edu.khtn.codefinder.adapters.CodeAdapter;
import edu.khtn.codefinder.database.CodeDatabase;

public class FinderFragment extends Fragment implements View.OnClickListener {
    FrameLayout layout;
    Button btnInsert, btnBack;
    CodeDatabase db;
    ListView listView;
    CodeAdapter adapter;
    List<Integer> showPriIdList;
    List<String> showNameList;
    List<Integer> PRI_ID_LIST = new ArrayList<>();
    List<String> TABLE_NAME_LIST = new ArrayList<>();
    int PARENT_ID = 0, CURRENT_LEVEL = 0;
    String CHOSEN_NAME = "", CHOSEN_ID = "", CURRENT_TABLE, CHILD_TABLE;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_finder, null, false);
        db = new CodeDatabase(getActivity());
        linkViewAndSetListener(view);
        createTableNamList(db.getReadableDatabase());
        Cursor cursor = db.selectTable(CURRENT_TABLE);
        createListView(cursor);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_insert:
                btnInsert();
                break;
            case R.id.btn_back:
                btnBack();
                break;
        }
    }

    private void btnBack() {
        if (CURRENT_LEVEL > 0) {
            PRI_ID_LIST.remove(CURRENT_LEVEL);
            CURRENT_LEVEL--;
            PARENT_ID = PRI_ID_LIST.get(CURRENT_LEVEL);
            CHILD_TABLE = CURRENT_TABLE;
            CURRENT_TABLE = TABLE_NAME_LIST.get(CURRENT_LEVEL);
            Cursor cursor = db.selectRecord
                    (CURRENT_TABLE, db.COL_PARENT_ID, PARENT_ID + "");
            createListView(cursor);
            CHOSEN_ID = "";
            CHOSEN_NAME = "";
        } else {
            HomeFragment fragment = new HomeFragment();
            FragmentTransaction FT = getFragmentManager().beginTransaction();
            FT.replace(R.id.activity_main, fragment);
            FT.commit();
        }
    }

    private void btnInsert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        final EditText editPopup = new EditText(getActivity());
        editPopup.setHint("Input name you want to create");
        dialog.setView(editPopup);
        dialog.setTitle("Create new");
        dialog.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String newText = editPopup.getText().toString();
                db.insertRecord(CURRENT_TABLE, newText, PARENT_ID, 0);
                Cursor cursor = db.selectRecord
                        (CURRENT_TABLE, db.COL_PARENT_ID, PARENT_ID + "");
                createListView(cursor);
                CHOSEN_ID = "";
                CHOSEN_NAME = "";
                dialog.cancel();
            }
        });
        dialog.show();
    }

    public void getChildListAndDelete(int currentLevel, String priIdOfParent) {
        try {
            currentLevel++;
            HashMap hashMap = new HashMap();
            Cursor cursor = db.selectRecord(TABLE_NAME_LIST.get(currentLevel),
                    db.COL_PARENT_ID, priIdOfParent);
            if (cursor != null) {
                cursor.moveToFirst();
                for (int i = 0; i < cursor.getCount(); i++) {
                    String priId = cursor.getString(db.PRI_ID_INDEX);
                    hashMap.put(i, priId);
                    cursor.moveToNext();
                }
            }
            deleteChild(currentLevel, hashMap);
        } catch (Exception e) {
        }
    }

    public boolean deleteChild(int currentLevel, HashMap hashMap) {
        try {
            for (int i = 0; i < hashMap.size(); i++) {
                String priId = (String) hashMap.get(i);
                db.deleteRecord(TABLE_NAME_LIST.get(currentLevel),
                        db.COL_PRI_ID, priId);
                getChildListAndDelete(currentLevel, priId);
            }
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    public void createTableNamList(SQLiteDatabase thisDB) {
        CURRENT_TABLE = db.TBL_HOME_NAME;
        TABLE_NAME_LIST.add(CURRENT_TABLE);
        PRI_ID_LIST.add(PARENT_ID);
        boolean inBound = true;
        int count = 1;
        do {
            try {
                db.selectTable(thisDB, db.TBL_SUB_NAME + count);
                TABLE_NAME_LIST.add(db.TBL_SUB_NAME + count);
                count++;
            } catch (Exception e) {
                inBound = false;
            }
        } while (inBound);
        if (CURRENT_LEVEL < TABLE_NAME_LIST.size() - 1) {
            CHILD_TABLE = TABLE_NAME_LIST.get(CURRENT_LEVEL + 1);
        } else {
            CHILD_TABLE = TABLE_NAME_LIST.get(CURRENT_LEVEL);
        }
    }

    public void createListView(Cursor cursor) {
        showNameList = new ArrayList();
        showPriIdList = new ArrayList();
        try {
            cursor.moveToFirst();
            do {
                showNameList.add(cursor.getString(db.NAME_INDEX));
                showPriIdList.add(cursor.getInt(db.PRI_ID_INDEX));
            } while (cursor.moveToNext());
        } catch (Exception e) {
        }
        adapter = new CodeAdapter(getActivity());
        adapter.setList(showNameList);
        listView = new ListView(getActivity());
        listView.setAdapter(adapter);
        layout.removeAllViews();
        layout.addView(listView);
        createOrEnterChildList();
    }

    public void createOrEnterChildList() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick
                    (AdapterView<?> parent, View view, int position, long id) {
                PARENT_ID = showPriIdList.get(position);
                PRI_ID_LIST.add(PARENT_ID);
                CURRENT_LEVEL++;
                CURRENT_TABLE = db.TBL_SUB_NAME + (CURRENT_LEVEL);
                if (CURRENT_LEVEL < TABLE_NAME_LIST.size() - 1) {
                    CHILD_TABLE = TABLE_NAME_LIST.get(CURRENT_LEVEL + 1);
                }
                String RELATIONSHIP = " REFERENCES " +
                        TABLE_NAME_LIST.get(CURRENT_LEVEL - 1) +
                        "(" + db.COL_PRI_ID + ")" +
                        " ON DELETE CASCADE NOT DEFERRABLE";
                db.createTableIfNotExist(CURRENT_TABLE, RELATIONSHIP);
                Cursor cursor = db.selectRecord
                        (CURRENT_TABLE, db.COL_PARENT_ID, PARENT_ID + "");
                createListView(cursor);
                CHOSEN_ID = "";
                CHOSEN_NAME = "";
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick
                    (AdapterView<?> parent, View view, int position, final long id) {
                CHOSEN_NAME = showNameList.get(position);
                CHOSEN_ID = showPriIdList.get(position).toString();
                PopupMenu popupMenu = new PopupMenu(getActivity(), view);
                popupMenu.inflate(R.menu.menu_context);
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener
                        (new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.menu_edit:
                                        btnUpdate();
                                        break;
                                    case R.id.menu_delete:
                                        btnDelete();
                                        break;
                                    case R.id.menu_cut:
                                        btnCut();
                                        break;
                                    case R.id.menu_copy:
                                        btnCopy();
                                        break;
                                    case R.id.menu_paste:
                                        btnPaste();
                                        break;
                                }
                                return true;
                            }
                        });
                return true;
            }
        });
    }

    private void btnUpdate() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        final EditText editPopup = new EditText(getActivity());
        editPopup.setText(CHOSEN_NAME);
        dialog.setView(editPopup);
        dialog.setTitle("Input to edit");
        dialog.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String newText = editPopup.getText().toString();
                db.updateRecord
                        (CURRENT_TABLE, newText, db.COL_NAME, CHOSEN_NAME);
                Cursor cursor = db.selectRecord
                        (CURRENT_TABLE, db.COL_PARENT_ID, PARENT_ID + "");
                createListView(cursor);
                CHOSEN_ID = "";
                CHOSEN_NAME = "";
                dialog.cancel();
            }
        });
        dialog.show();
    }

    private void btnDelete() {
        getChildListAndDelete(CURRENT_LEVEL, CHOSEN_ID);
        db.deleteRecord(CURRENT_TABLE, db.COL_PRI_ID, CHOSEN_ID);
        Cursor cursor = db.selectRecord
                (CURRENT_TABLE, db.COL_PARENT_ID, PARENT_ID + "");
        createListView(cursor);
        CHOSEN_ID = "";
        CHOSEN_NAME = "";
    }

    private void btnCut() {

    }

    private void btnCopy() {

    }

    private void btnPaste() {

    }

    public void linkViewAndSetListener(View view) {
        layout = (FrameLayout) view.findViewById(R.id.layout_list);
        btnInsert = (Button) view.findViewById(R.id.btn_insert);
        btnInsert.setOnClickListener(this);
        btnBack = (Button) view.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
    }

    public void toast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

}
