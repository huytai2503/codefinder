package edu.khtn.codefinder.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import edu.khtn.codefinder.R;

public class HomeFragment extends Fragment implements View.OnClickListener {
    public static final String HOME = "home";
    public static final String databaseDir = "/data/data/edu.khtn.codefinder/databases/";
    public static final File extRootDir = Environment.getExternalStorageDirectory();
    public static final File externalDB = new File(extRootDir, "CodeDatabase.sqlite");
    public static final File internalDB = new File(databaseDir, "CodeDatabase.sqlite");
    Button btnOpenList, btnImport, btnExport;

    @Nullable
    @Override
    public View onCreateView
            (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, null, false);
        btnOpenList = (Button) view.findViewById(R.id.btn_searchList);
        btnOpenList.setOnClickListener(this);
        btnImport = (Button) view.findViewById(R.id.btn_importList);
        btnImport.setOnClickListener(this);
        btnExport = (Button) view.findViewById(R.id.btn_exportList);
        btnExport.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        if (v == btnOpenList) {
            openFinder(getFragmentManager());
        } else if (v == btnExport) {
            if (copyFile(getActivity(),internalDB, externalDB)) {
                toast(getActivity(),"Copied: " + internalDB.getName() + " out SDcard");
            }
        } else if (v == btnImport) {
            if (copyFile(getActivity(), externalDB, internalDB)) {
                toast(getActivity(), "Copied: " + internalDB.getName() + " in phone");
            }
        }
    }

    public static boolean copyFile(Activity activity, File fromFile, File toFile) {
        InputStream in = null;
        OutputStream out = null;
        try{
            in = new FileInputStream(fromFile);
            out = new FileOutputStream(toFile);
            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            return true;
        }catch (Exception e){
            toast(activity, "Copy failed");
        }finally {
            try{
                if (in != null)
                    in.close();
                if (out != null)
                    out.close();
            }catch (Exception e){
            }
        }return false;
    }

    public static void openFinder(FragmentManager manager) {
        FinderFragment fragment = new FinderFragment();
        FragmentTransaction FT = manager.beginTransaction();
        FT.replace(R.id.activity_main, fragment);
        FT.addToBackStack(HOME);
        FT.commit();
    }

    public static void toast(Activity activity, String msg){
        Toast.makeText(activity.getBaseContext(),
                msg.toString(),Toast.LENGTH_LONG).show();
    }
}
