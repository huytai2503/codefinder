package edu.khtn.codefinder;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import edu.khtn.codefinder.database.CodeDatabase;
import edu.khtn.codefinder.fragments.HomeFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        HomeFragment fragment = new HomeFragment();
        addFragment(fragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_home:
                HomeFragment fragment = new HomeFragment();
                addFragment(fragment);
                break;
            case R.id.menu_finder:
                HomeFragment.openFinder(getFragmentManager());
                break;
            case R.id.menu_import:
                HomeFragment.copyFile
                        (this, HomeFragment.externalDB, HomeFragment.internalDB);
                toast("Đã copy: " + CodeDatabase.DATABASE_NAME + " vào máy");
                HomeFragment.openFinder(getFragmentManager());
                break;
            case R.id.menu_export:
                HomeFragment.copyFile
                        (this,HomeFragment.internalDB,HomeFragment.externalDB);
                toast("Đã copy: " + CodeDatabase.DATABASE_NAME + " ra SDcard");
                break;
            case R.id.menu_search:
                break;
        }
        return true;
    }

    public void addFragment(Fragment fragment){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.activity_main, fragment);
        transaction.commit();
    }

    public void toast(String msg){
        Toast.makeText(getBaseContext(),msg,Toast.LENGTH_SHORT).show();
    }
}
